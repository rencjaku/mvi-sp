# Biomedical image segmentation

Biomedical segmentation is the process of segmenting and detecting different anatomical structures in 2D/3D images. Because there is a lack of biomedical images for ML algorithms, the models that specialize in biomedical segmentation need a few samples to work efficiently.

## Task

The goal of this project is to successfully segment data from lung CT scans using deep learning networks.

## Model

For image segmentation, I chose UNet architecture due to its performance on smaller datasets (1). UNet is a multilayer convolutional network with architecture resembling an autoencoder.

<p align="middle"> 
<img src="unet.png" width=512 hspace="20" wspace="20">
</p>

I've used the original architecture as described in (1) - 4 encoder blocks connected with 4 decoder blocks using skip connections and bottleneck. Each encoder block consists of 2 convolutions and a maxpool operation. Similarly, the encoder blocks consist of a transpose operation followed by 2 convolutions.

## Data

For the dataset, I chose covid-19 CT scan lesion dataset (2) from Kaggle. This dataset consists of over 2700 lesion images and corresponding masks. Since I wanted to simulate a real situation, I've picked 110 images and their masks to use in this project.

These 110 images come from 5 different patients (22 from each) and resemble different stages of leison development.

## Usage

In order to use the scripts, it is possible to download both input data and model checkpoints from https://drive.google.com/drive/u/1/folders/1Zdb7oIrF6f0K824FFhic5unKZcMuzM0S.

* For data augmentation run `python3 codes/data_aug.py.`
* For model training run `python3 codes/train.py`.
* To generate masks run `python3 codes/test.py`.

Or just use *run_unet.iypnb* for inspiration.


## Resources

[1] Ronneberger, O., Fisher, P., Brox, T.: U-Net: Convolutional Networks for Biomedical Image Segmentation. In: CoRR (2015). DOI: https://doi.org/10.48550/arXiv.1505.04597

[2] COVID-19 CT scan lesion segmentation dataset. https://www.kaggle.com/datasets/maedemaftouni/covid19-ct-scan-lesion-segmentation-dataset
